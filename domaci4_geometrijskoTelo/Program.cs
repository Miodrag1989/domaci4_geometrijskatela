﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_geometrijskoTelo
{
    class Program
    {
        static void Main(string[] args)
        {
            Lopta[] nizLopti = new Lopta[2];
            for (int i = 0; i < nizLopti.Length; i++)
            {
                nizLopti[i] = new Lopta();
                nizLopti[i].Ucitavanje();
            }

            Valjak[] nizValjkova = new Valjak[3];

            for(int i = 0; i < nizValjkova.Length; i++)
            {
                nizValjkova[i] = new Valjak();
                nizValjkova[i].Ucitavanje();
            }

            Console.WriteLine("Unete lopte!");
            foreach (Lopta l in nizLopti)
            {
                l.Prikazivanje();
            }
            Console.WriteLine();
            Console.WriteLine("Uneti valjkovi!");

            foreach(Valjak v in nizValjkova)
            {
                v.Prikazivanje();
            }
        }
    }
}
