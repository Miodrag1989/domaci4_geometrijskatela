﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_geometrijskoTelo
{
    class Lopta : IGeometrijskoTelo
    {
        protected float poluprecnik;
       
        public float Povrsina()
        {
            return 4 * (float)Math.Pow(poluprecnik, 2) * (float)Math.PI;
        }

        public void Prikazivanje()
        {
            Console.WriteLine($"Lopta poluprecnika {this.poluprecnik} ima povrsinu {this.Povrsina()} i zapreminu {this.Zapremina()}");
        }

        public void Ucitavanje()
        {
            Console.WriteLine("Ucitavanje nove lopte!");
            Console.WriteLine("Uneti vrednost poluprecnika lopte!");
            float.TryParse(Console.ReadLine(), out this.poluprecnik);
            
        }

        public float Zapremina()
        {
            return (4 * (float)Math.Pow(poluprecnik, 3) * (float)Math.PI) / 3;
        }
    }
}
