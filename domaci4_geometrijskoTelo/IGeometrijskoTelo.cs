﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_geometrijskoTelo
{
    interface IGeometrijskoTelo
    {
        float Povrsina();
        float Zapremina();
        void Ucitavanje();
        void Prikazivanje();
    }
}
