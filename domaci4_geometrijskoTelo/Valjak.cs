﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_geometrijskoTelo
{
    class Valjak : IGeometrijskoTelo
    {
        protected float poluprecnik;
        protected float visina;
        public float Povrsina()
        {
            return 2 * ((float)Math.Pow(poluprecnik, 2) * (float)Math.PI) + (2 * (float)Math.PI * this.poluprecnik * this.visina);
        }

        public void Prikazivanje()
        {
            Console.WriteLine($"Valjak poluprecnika {this.poluprecnik} i visine {this.visina} ima povrsinu {this.Povrsina()} i zapreminu {this.Zapremina()}");
        }

        public void Ucitavanje()
        {
            Console.WriteLine("Ucitavanje novog valjka!");
            Console.WriteLine("Uneti vrednost poluprecnika osnove valjka!");
            float.TryParse(Console.ReadLine(), out this.poluprecnik);
            Console.WriteLine("Uneti vrednost visine valjka!");
            float.TryParse(Console.ReadLine(), out this.visina);
        }

        public float Zapremina()
        {
            return (float)(Math.Pow(poluprecnik, 2) * Math.PI * this.visina);
        }
    }
}
